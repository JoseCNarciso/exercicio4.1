package pacote;

public class Carro {


    private String modelo;
    private int anoFabricacao;
    private int km;
    private String combustivel;
    private int quantidadePortas;
    private String cor;
    private String acabamentoInterior;
    private int numeroChassis;
    private double valor;


    public Carro() {
        cor = "Branco";
    }


    public Carro( String modelo, int anoFabricacao, int km, String combustivel, int quantidadePortas, String cor, String acabamentoInterior, int numeroChassis, double valor) {
        this.modelo = modelo;
        this.anoFabricacao = anoFabricacao;
        this.combustivel = combustivel;
        this.quantidadePortas = quantidadePortas;
        this.cor = cor;
        this.acabamentoInterior = acabamentoInterior;
        this.numeroChassis = numeroChassis;
        this.valor = valor ;
        this.km = km;

    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo( String modelo ) {
        this.modelo = modelo;
    }

    public int getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao( int anoFabricacao ) {
        this.anoFabricacao = anoFabricacao;
    }

    public int getKm() {
        return km;
    }

    public void setKm( int km ) {
        this.km = km;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel( String combustivel ) {
        this.combustivel = combustivel;
    }

    public int getQuantidadePortas() {
        return quantidadePortas;
    }

    public void setQuantidadePortas( int quantidadePortas ) {
        this.quantidadePortas = quantidadePortas;
    }

    public String getCor() {
        return cor;
    }

    public void setCor( String cor ) {
        this.cor = cor;
    }

    public String getAcabamentoInterior() {
        return acabamentoInterior;
    }

    public void setAcabamentoInterior( String acabamentoInterior ) {
        this.acabamentoInterior = acabamentoInterior;
    }

    public int getNumeroChassis() {
        return numeroChassis;
    }

    public void setNumeroChassis( int numeroChassis ) {
        this.numeroChassis = numeroChassis;
    }

    public double getValor() {
        return valor;
    }

    public void setValor( double valor ) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Carro{" +
                "modelo='" + modelo + '\'' +
                ", anoFabricacao=" + anoFabricacao +
                ", km=" + km +
                ", combustivel='" + combustivel + '\'' +
                ", quantidadePortas=" + quantidadePortas +
                ", cor='" + cor + '\'' +
                ", acabamentoInterior='" + acabamentoInterior + '\'' +
                ", numeroChassis=" + numeroChassis +
                ", valor=" + valor +
                '}';
    }
}
